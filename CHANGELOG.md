# CHANGELOG.md

## Unreleased

## 0.1.3

Ewoks tasks

- `EmailTask`: send an e-mail with `smtplib.SMTP`.
- `PublishProcessedDataFolderTask`: publish a processed dataset to icat.
- `ToneTask`: make a sound with STDOUT message.
- `WaiterTask`: sleep for a number of seconds.
- `GetFromDataTask`: extract a value from a dictionary or list.
