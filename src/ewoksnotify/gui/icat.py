import platform
from silx.gui import qt
from ewoksorange.gui.parameterform import block_signals
from ewoksnotify.gui.utils.buttons import PadlockButton
from ewoksnotify.utils.beamlines import ALL_BEAMLINES


class PublishProcessedDataWidget(qt.QWidget):
    sigConfigChanged = qt.Signal()
    """emit when the configuration changed"""

    _TOOLTIP_PAD_LOCKS = "when receive a new scan the value will be updated if valid informations are found. If the field is lock then no update will be done automatically."

    def __init__(self, parent=None, beamlines: tuple = ALL_BEAMLINES) -> None:
        super().__init__(parent)
        self.setLayout(qt.QGridLayout())

        # beamline
        self._beamlineCB = qt.QComboBox(self)
        self._beamlineCB.setEditable(True)
        self._beamlineCB.addItems(beamlines)
        self._beamlineCB.lineEdit().setPlaceholderText("beamline name")
        self._beamlineLabel = qt.QLabel("beamline", self)
        self.layout().addWidget(
            self._beamlineLabel,
            0,
            0,
            1,
            1,
        )
        self.layout().addWidget(
            self._beamlineCB,
            0,
            1,
            1,
            1,
        )
        self._beamlinePLB = PadlockButton(self)
        self._beamlinePLB.setToolTip(self._TOOLTIP_PAD_LOCKS)
        self.layout().addWidget(
            self._beamlinePLB,
            0,
            2,
            1,
            1,
        )

        # proposal
        self._proposalQLE = qt.QLineEdit("", self)
        self._proposalQLE.setPlaceholderText("proposal name")
        self._proposalLabel = qt.QLabel("proposal", self)
        self.layout().addWidget(
            self._proposalLabel,
            1,
            0,
            1,
            1,
        )
        self.layout().addWidget(
            self._proposalQLE,
            1,
            1,
            1,
            1,
        )
        self._proposalPLB = PadlockButton(self)
        self._proposalPLB.setToolTip(self._TOOLTIP_PAD_LOCKS)
        self.layout().addWidget(
            self._proposalPLB,
            1,
            2,
            1,
            1,
        )

        # dataset
        self._datasetQLE = qt.QLineEdit("", self)
        self._datasetQLE.setPlaceholderText("dataset name")
        self._datasetLabel = qt.QLabel("dataset", self)
        self.layout().addWidget(
            self._datasetLabel,
            2,
            0,
            1,
            1,
        )
        self.layout().addWidget(
            self._datasetQLE,
            2,
            1,
            1,
            1,
        )
        self._datasetPLB = PadlockButton(self)
        self._datasetPLB.setToolTip(self._TOOLTIP_PAD_LOCKS)
        self.layout().addWidget(
            self._datasetPLB,
            2,
            2,
            1,
            1,
        )

        # set up
        default_beamline = self.getDefaultBeamline(available_beamlines=beamlines)
        if default_beamline is not None:
            self._beamlineCB.setCurrentText(default_beamline)

        # connect signal / slot
        self._beamlinePLB.toggled.connect(self._configChanged)
        self._proposalPLB.toggled.connect(self._configChanged)
        self._datasetPLB.toggled.connect(self._configChanged)
        self._beamlineCB.currentTextChanged.connect(self._configChanged)
        self._proposalQLE.textEdited.connect(self._configChanged)
        self._datasetQLE.textEdited.connect(self._configChanged)

    @staticmethod
    def getDefaultBeamline(available_beamlines: tuple):
        """
        function to try to guess current beamline according to host name.
        Make user win some time if we are lucky...
        """
        hostname = platform.node()
        for beamline in available_beamlines:
            if beamline in hostname:
                return beamline

        return None

    def set_auto_update(self, update: bool):
        self._beamlinePLB.setChecked(not update)
        self._proposalPLB.setChecked(not update)
        self._datasetPLB.setChecked(not update)

    def _configChanged(self, *args, **kwargs):
        self.sigConfigChanged.emit()

    def getConfiguration(self) -> dict:
        return {
            "beamline_auto_update": not self._beamlinePLB.isChecked(),
            "dataset_auto_update": not self._datasetPLB.isChecked(),
            "proposal_auto_update": not self._proposalPLB.isChecked(),
            "beamline": self._beamlineCB.currentText(),
            "dataset": self._datasetQLE.text(),
            "proposal": self._proposalQLE.text(),
        }

    def setConfiguration(self, configuration: dict):
        with block_signals(self):
            # handle beamline
            beamline = configuration.get("beamline", None)
            if beamline is not None:
                self._beamlineCB.setCurrentText(beamline)
            beamline_auto_update = configuration.get("beamline_auto_update", None)
            if beamline_auto_update is not None:
                self._beamlinePLB.setChecked(not beamline_auto_update)

            # handle dataset
            dataset = configuration.get("dataset", None)
            if dataset is not None:
                self._datasetQLE.setText(dataset)
            dataset_auto_update = configuration.get("dataset_auto_update", None)
            if dataset_auto_update is not None:
                self._datasetPLB.setChecked(not dataset_auto_update)

            # handle proposal
            proposal = configuration.get("proposal", None)
            if proposal is not None:
                self._proposalQLE.setText(proposal)
            proposal_auto_update = configuration.get("proposal_auto_update", None)
            if proposal_auto_update is not None:
                self._proposalPLB.setChecked(not proposal_auto_update)

        self._configChanged()

    def setDatasetRowVisible(self, visible: bool) -> None:
        for widget in (self._datasetLabel, self._datasetPLB, self._datasetQLE):
            widget.setVisible(visible)

    def setBeamlineRowVisible(self, visible: bool) -> None:
        for widget in (self._beamlineLabel, self._beamlinePLB, self._beamlineCB):
            widget.setVisible(visible)

    def setProposalRowVisible(self, visible: bool) -> None:
        for widget in (self._proposalLabel, self._proposalPLB, self._proposalQLE):
            widget.setVisible(visible)
