tone notifier
"""""""""""""

This task allow you to get some notification when an object pass through it.

The input object will simply be passed to the next task if connected.

.. image:: img/workflow_with_tone_notification.png
