# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

from importlib.metadata import version as get_version


project = "ewoksnotify"
release = get_version(project)
version = ".".join(release.split(".")[:2])
copyright = "2024, ESRF"
author = "ESRF"
docstitle = f"{project} {version}"

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.autosummary",
    "sphinx.ext.intersphinx",
    "nbsphinx",
    "sphinx_design",
]
templates_path = ["_templates"]
exclude_patterns: list = []

autosummary_generate = True
autodoc_default_flags = [
    "members",
    "undoc-members",
    "show-inheritance",
]

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = "pydata_sphinx_theme"
html_static_path: list = []
html_logo = "img/icon_medium.svg"
html_theme_options = {
    "icon_links": [
        {
            "name": "gitlab",
            "url": "https://gitlab.esrf.fr/workflow/ewoks/ewoksnotify",
            "icon": "fa-brands fa-gitlab",
        },
        {
            "name": "pypi",
            "url": "https://pypi.org/project/ewoksnotify",
            "icon": "fa-brands fa-python",
        },
    ],
    "navbar_start": ["navbar-logo", "navbar_start"],
    "footer_start": ["copyright"],
    "footer_end": ["footer_end"],
}

# -- Options for other output -------------------------------------------------
root_doc = "index"
man_pages = [(root_doc, project, "ewoksnotify Documentation", [author], 1)]
htmlhelp_basename = project
htmlhelp_pages = [(root_doc, project, "ewoksnotify Add-on Documentation", [author], 1)]
